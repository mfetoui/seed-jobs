def repository = 'https://gitlab.com/mfetoui/MsCatalog.git'                            
def credentialsId = 'gitlab-jenkins'

pipelineJob('MsCatalog Dev') {
  definition {
    cpsScm {
      scm {
        git {
          remote {
            url(repository)
            credentialsId(credentialsId)
          }
          branch('dev')
          scriptPath('Jenkinsfile')
        }
      }
    }
  }
}
