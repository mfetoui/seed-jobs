def repository          = 'https://github.com/MohsineF/MsCatalog.git'
def credentialsId       = 'gitlab-jenkins'

pipelineJob('MsCatalog Recette') {
  definition {
    cpsScm {
      scm {
        git {
          remote {
            url(repository)
            credentialsId(credentialsId)
          }
          branch('PRP')
          scriptPath('Jenkinsfile')
        }
      }
    }
  }
}
